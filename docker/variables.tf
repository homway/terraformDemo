variable "container_name" {
  description = "Value of the name for the Docker container"
  type        = string
  default     = "ExampleNginxContainer"
}

variable "nginx_port" {
  type        = number
  default     = 8080
  description = "nginx port"
}