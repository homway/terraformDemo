terraform {
  required_providers {
    gcp = {
      source  = "hashicorp/google",
      version = "4.15.0",
    }
  }
}

provider "gcp" {
  credentials = "${file("golden-sandbox-342310-1533faee930f.json")}"
  project = "golden-sandbox-342310"
  region = "asia-east1"
  zone = "asia-east1-b"
}

resource "google_compute_network" "abc_network" {
  name = "abc-network"
}

resource "google_compute_instance" "abc_instance" {
  name         = "abc-instance"
  machine_type = "f1-micro"
  count = 1
  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-9"
    }
  }


  scheduling {
    preemptible = true
    automatic_restart = false
  }
  network_interface {
    network = google_compute_network.abc_network.name
    access_config {
    }
  }
}
